from flask import Flask
import sys
import os

# create the app
# set the static folder and instance folder
app = Flask(__name__)

# register the blueprint
@app.route('/')
def index():
	return "hello"

app.run()