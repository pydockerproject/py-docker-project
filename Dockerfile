FROM python:3.4
ADD . /app
WORKDIR /app
RUN pip install flask
CMD python3 run.py